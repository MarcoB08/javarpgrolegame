package org.example.items;

public abstract class Item {
    /* Creating private variables */
    private String name;
    private int requiredLevel;
    private Slot itemSlot;

     /* Creates a constructor extends Weapon and Armor */
    public Item(String name, int requiredLevel, Slot itemSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Slot getItemSlot() {
        return itemSlot;
    }

    public void setItemSlot(Slot itemSlot) {
        this.itemSlot = itemSlot;
    }

    public String getName() {
        return name;
    }
}
