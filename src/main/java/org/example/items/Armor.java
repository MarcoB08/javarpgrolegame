package org.example.items;

import org.example.heroes.HeroAttribute;

/* Subclass to the Item Class */
public class Armor extends Item {
    private ArmorType armorType;
    private HeroAttribute armorAttributes;

    /* Constructor for Armor */
    public Armor(String name, int requiredLevel, Slot itemSlot, ArmorType armorType, int strength, int dexterity , int intelligence) {
        super(name, requiredLevel, itemSlot);
        this.armorType = armorType;
        this.armorAttributes = new HeroAttribute(strength, dexterity, intelligence);
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public HeroAttribute getArmorAttributes() {
        return armorAttributes;
    }
}
