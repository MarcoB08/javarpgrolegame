package org.example.heroes;

import org.example.items.ArmorType;
import org.example.items.WeaponType;

import java.util.List;

/* Subclass to the Hero class */
public class Warrior extends Hero{
    /* Variables to hardcode the HeroAttributes */
    private final HeroAttribute beginAttributes = new HeroAttribute(5,2,1);
    private final HeroAttribute levelingAttributes = new HeroAttribute(3,2,1);

    /* Constructor that sets the beginHeroAttributes and what kind of weapon and armor the subclass should wear */
    public Warrior(String name) {
        super(name);
        super.setLevelAttributes(beginAttributes);
        super.setValidWeaponTypes(List.of(WeaponType.Axes, WeaponType.Hammers, WeaponType.Swords));
        super.setValidArmorTypes(List.of(ArmorType.Mail, ArmorType.Plate));
    }

    /* Method that level up the Hero and set the new HeroAttributes */
    @Override
    public void levelUp(){
        setLevel(getLevel() + 1);
        getLevelAttributes().add(levelingAttributes);
    }

    /* Method that calculate the HerodDamage with the amount levelAttribute of Strength */
    @Override
    protected double calculateHeroDamage(double weaponDamage) {
        return weaponDamage * (1 + ((double) getLevelAttributes().getStrength() / 100));
    }
}

