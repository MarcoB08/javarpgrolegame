package org.example.heroes;

public class HeroAttribute {
    /* Creating private variables. */
    private int strength;
    private int dexterity;
    private int intelligence;

    /* Creates a constructor inherited the HeroAttributes */
    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /* Method to merge the HeroAttributes */
    public void add(HeroAttribute levelingAttribute){
        this.strength += levelingAttribute.getStrength();
        this.dexterity += levelingAttribute.getDexterity();
        this.intelligence += levelingAttribute.getIntelligence();
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
