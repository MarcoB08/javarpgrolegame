package org.example.heroes;

import org.example.InvalidArmorException;
import org.example.InvalidWeaponException;
import org.example.items.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {
    /* Creating private variables */
    private final String name;
    private int level = 1;
    private HeroAttribute levelAttributes;
    private List<WeaponType> validWeaponTypes;
    private List<ArmorType> validArmorTypes;
    private final HashMap<Slot, Item> equipment = new HashMap<>();

    /* Creates a constructor extends Mage,Ranger,Rogue and Warrior
    * Also created its own slots for the equipment of the Hero
    * */
    public Hero(String name) {
        this.name = name;
        for (Slot itemSlot: Slot.values()){
            equipment.put(itemSlot,null);
        }
    }

    /*Abstract functions they overwrite by the subclasses Mage,Ranger,Rogue and Warrior */
    public abstract void levelUp();
    protected abstract double calculateHeroDamage(double weaponDamage);

    /* Method that calculate the Total Attributes of the Hero
    *  currenHero variables contains the HeroAttributes of the subClasses
    *  The loop checks if the equipment Hashmap get an armor slot and have an item in the slot.
    *  calculateArmorAttributes variables contains the ArmorAttributes of all items in the equipment Hashmap
    *  Returns the new HeroAttributes of the Hero
    * */
    public HeroAttribute CalculateTotalAttributes() {
        int currentHeroStrength = this.levelAttributes.getStrength();
        int currentHeroDexterity = this.levelAttributes.getDexterity();
        int currentHeroIntelligence = this.levelAttributes.getIntelligence();

        for(Map.Entry<Slot, Item> set: equipment.entrySet()){
            if (set.getKey() != Slot.Weapon && set.getValue() != null){
                HeroAttribute calculateArmorAttributes =((Armor)set.getValue()).getArmorAttributes();
                currentHeroStrength += calculateArmorAttributes.getStrength();
                currentHeroDexterity += calculateArmorAttributes.getDexterity();
                currentHeroIntelligence += calculateArmorAttributes.getIntelligence();
            }
        }
        return new HeroAttribute(currentHeroStrength, currentHeroDexterity,currentHeroIntelligence);
    }

    /* Method that equips a weapon to the weaponSlot of the Hero
    *  It checks to the requiredLevel and the WeaponType of the Weapon.
    *  The subclasses decide what kind of weapon the hero can equip.
    * */
    public void equipWeapon(Weapon newWeapon) throws InvalidWeaponException {
        if (newWeapon.getRequiredLevel() > this.level || !this.validWeaponTypes.contains(newWeapon.getWeaponType())) {
            throw new InvalidWeaponException("Invalid new weapon/Weapon is not your level");
        }
        this.equipment.put(Slot.Weapon,newWeapon);
    }

    /* Method that equips a ArmorPiece to one of the ArmorsSlot of the Hero
     * It checks to the requiredLevel and the ArmorType of the armor.
     * The subclasses decide what kind of Armor the hero can equip.
     * */
    public void equipArmor(Slot armor, Armor newArmor) throws InvalidArmorException{
        if (newArmor.getRequiredLevel() > this.level || !this.validArmorTypes.contains(newArmor.getArmorType())){
            throw new InvalidArmorException("Invalid  new armor/Weapon is not your level");
        }
        this.equipment.put(armor,newArmor);
    }

    /* Method to display all stats of the Hero */
    public String displayHeroStats(){
        StringBuilder heroStats = new StringBuilder();
        heroStats.append("Hero Name: " + getName());
        heroStats.append("\nHero Level " + getLevel());
        heroStats.append("\nHero Strength " +getLevelAttributes().getStrength());
        heroStats.append("\nHero Dexterity " + getLevelAttributes().getDexterity());
        heroStats.append("\nHero Intelligence " + getLevelAttributes().getIntelligence());
        heroStats.append("\nHero Damage " + getHeroDamage());
        return heroStats.toString();
    }

    /* Gets the WeaponDamage of the weapon
    *  Checks if the Hero is equipped with a weapon and returns the damage of the weapon
    * */
    public double getWeaponDamage(){
        Weapon weapon = (Weapon) equipment.get(Slot.Weapon);
        if (weapon == null) return calculateHeroDamage(1);
        return weapon.getWeaponDamage();
    }

    /* Gets the calculated WeaponDamage with the attributes of the subclasses */
    public double getHeroDamage(){
        double weaponDamage = getWeaponDamage();
        return Math.round(calculateHeroDamage(weaponDamage) * 100) / 100D;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }

    public void setLevelAttributes(HeroAttribute levelAttributes) {
        this.levelAttributes = levelAttributes;
    }

    public void setValidWeaponTypes(List<WeaponType> validWeaponTypes) {
        this.validWeaponTypes = validWeaponTypes;
    }

    public void setValidArmorTypes(List<ArmorType> validArmorTypes) {
        this.validArmorTypes = validArmorTypes;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }
}
