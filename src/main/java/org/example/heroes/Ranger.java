package org.example.heroes;

import org.example.items.ArmorType;
import org.example.items.WeaponType;

import java.util.List;

/* Subclass to the Hero class */
public class Ranger extends Hero{
    /* Variables to hardcode the HeroAttributes */
    private final HeroAttribute beginAttributes = new HeroAttribute(1,7,1);
    private final HeroAttribute levelingAttributes = new HeroAttribute(1,5,1);

    /* Constructor that sets the beginHeroAttributes and what kind of weapon and armor the subclass should wear */
    public Ranger(String name) {
        super(name);
        super.setLevelAttributes(beginAttributes);
        super.setValidWeaponTypes(List.of(WeaponType.Bows));
        super.setValidArmorTypes(List.of(ArmorType.Leather, ArmorType.Mail));
    }

    /* Method that level up the Hero and set the new HeroAttributes */
    @Override
    public void levelUp(){
        setLevel(getLevel() + 1);
        getLevelAttributes().add(levelingAttributes);
    }

    /* Method that calculate the HerodDamage with the amount levelAttribute of Dexterity */
    @Override
    protected double calculateHeroDamage(double weaponDamage) {
        return weaponDamage * (1 + ((double) getLevelAttributes().getDexterity() / 100));
    }
}
