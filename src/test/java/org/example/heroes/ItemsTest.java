package org.example.heroes;

import org.example.InvalidArmorException;
import org.example.InvalidWeaponException;
import org.example.items.*;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ItemsTest{

    @Test
    public void WeaponCreated_EquipWeapon_ShouldBeCreatedCorrectly(){
        Weapon firstWeaponCreated = new Weapon("Clam-hammer",1,WeaponType.Hammers, 60);

        Assert.assertEquals("Clam-hammer", firstWeaponCreated.getName());
        Assert.assertEquals(1, firstWeaponCreated.getRequiredLevel());
        Assert.assertEquals(WeaponType.Hammers, firstWeaponCreated.getWeaponType());
        Assert.assertEquals(60, firstWeaponCreated.getWeaponDamage());
    }
    @Test
    public void ArmorCreated_EquipArmor_ShouldBeCreatedCorrectly(){
        Armor firstArmorCreated = new Armor("Iron plate", 1, Slot.Body,ArmorType.Plate,6,1,1 );

        Assert.assertEquals("Iron plate", firstArmorCreated.getName());
        Assert.assertEquals(1, firstArmorCreated.getRequiredLevel());
        Assert.assertEquals(Slot.Body, firstArmorCreated.getItemSlot());
        Assert.assertEquals(ArmorType.Plate, firstArmorCreated.getArmorType());
        Assert.assertEquals(6,firstArmorCreated.getArmorAttributes().getStrength());
        Assert.assertEquals(1,firstArmorCreated.getArmorAttributes().getDexterity());
        Assert.assertEquals(1,firstArmorCreated.getArmorAttributes().getIntelligence());
    }

    @Test
    public void HeroEquipWeapon_RequiredLevel_ShouldBeCheckRequiredLevelCorrectly()  {
        Hero myFirstMageHero =  new Mage("Marco");
        Weapon myFirstMageWeapon = new Weapon("woodenWand", 2, WeaponType.Wands,20);
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> myFirstMageHero.equipWeapon(myFirstMageWeapon));

        assertEquals("Invalid new weapon/Weapon is not your level", exception.getMessage());
    }
    @Test
    public void HeroEquipWeapon_WeaponType_ShouldBeCheckWeaponTypeCorrectly()  {
        Hero myFirstMageHero =  new Mage("Marco");
        Weapon myFirstMageWeapon = new Weapon("BlightStrike", 1, WeaponType.Swords,20);
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> myFirstMageHero.equipWeapon(myFirstMageWeapon));

        assertEquals("Invalid new weapon/Weapon is not your level", exception.getMessage());
    }

    @Test
    public void HeroEquipArmor_RequiredLevel_ShouldBeCheckRequiredLevelCorrectly(){
        Hero myFirstWarriorHero =  new Warrior("Marco");
        Armor myFirstWarriorArmor = new Armor("Golden Plate",2,Slot.Body,ArmorType.Plate,8,2,2);
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> myFirstWarriorHero.equipArmor(Slot.Body,myFirstWarriorArmor));

        assertEquals("Invalid  new armor/Weapon is not your level", exception.getMessage());
    }
    @Test
    public void HeroEquipArmor_RequiredArmorType_ShouldBeCheckArmorTypeCorrectly(){
        Hero myFirstWarriorHero =  new Warrior("Marco");
        Armor myFirstWarriorArmor = new Armor("Cloak",1,Slot.Body,ArmorType.Cloth,1,8,5);
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> myFirstWarriorHero.equipArmor(Slot.Body,myFirstWarriorArmor));

        assertEquals("Invalid  new armor/Weapon is not your level", exception.getMessage());
    }

    @Test
    public void TotalArmorAttributes_WithNoEquipment_ShouldBeCalculatedCorrectly() throws InvalidArmorException {
        Hero myFirstMageHero =  new Mage("Marco");
        HeroAttribute calculatedAttributes = myFirstMageHero.CalculateTotalAttributes();
        HeroAttribute expectedHero = new HeroAttribute(1,1,8);

        Assert.assertEquals(expectedHero.getStrength(), calculatedAttributes.getStrength());
        Assert.assertEquals(expectedHero.getDexterity(), calculatedAttributes.getDexterity());
        Assert.assertEquals(expectedHero.getIntelligence(), calculatedAttributes.getIntelligence());
    }

    @Test
    public void TotalArmorAttributes_WithOneEquipments_ShouldBeCalculatedCorrectly() throws InvalidArmorException {
        Hero myFirstMageHero =  new Mage("Marco");
        Armor armor1 = new Armor("Cloak",1,Slot.Body,ArmorType.Cloth,1,5,7);
        myFirstMageHero.equipArmor(Slot.Body, armor1);
        HeroAttribute calculatedAttributes = myFirstMageHero.CalculateTotalAttributes();
        HeroAttribute expectedHero = new HeroAttribute(2,6,15);

        Assert.assertEquals(expectedHero.getStrength(), calculatedAttributes.getStrength());
        Assert.assertEquals(expectedHero.getDexterity(), calculatedAttributes.getDexterity());
        Assert.assertEquals(expectedHero.getIntelligence(), calculatedAttributes.getIntelligence());

    }


    @Test
    public void TotalArmorAttributes_WithTwoEquipments_ShouldBeCalculatedCorrectly() throws InvalidArmorException {
        Hero myFirstMageHero =  new Mage("Marco");
        Armor armor1 = new Armor("Cloak",1,Slot.Body,ArmorType.Cloth,1,5,7);
        Armor armor2 = new Armor("flip flops",1,Slot.Legs,ArmorType.Cloth,1,2,5);
        myFirstMageHero.equipArmor(Slot.Body, armor1);
        myFirstMageHero.equipArmor(Slot.Legs, armor2);
        HeroAttribute calculatedAttributes = myFirstMageHero.CalculateTotalAttributes();
        HeroAttribute expectedHero = new HeroAttribute(3,8,20);

        Assert.assertEquals(expectedHero.getStrength(), calculatedAttributes.getStrength());
        Assert.assertEquals(expectedHero.getDexterity(), calculatedAttributes.getDexterity());
        Assert.assertEquals(expectedHero.getIntelligence(), calculatedAttributes.getIntelligence());

    }
    @Test
    public void TotalArmorAttributes_WithThreeEquipments_ShouldBeCalculatedCorrectly() throws InvalidArmorException {
        Hero myFirstMageHero =  new Mage("Marco");
        Armor armor1 = new Armor("Cloak",1,Slot.Body,ArmorType.Cloth,1,5,7);
        Armor armor2 = new Armor("flip flops",1,Slot.Legs,ArmorType.Cloth,1,2,5);
        Armor armor3 = new Armor("Wooden Crown",1,Slot.Head,ArmorType.Cloth,1,4,9);
        myFirstMageHero.equipArmor(Slot.Body, armor1);
        myFirstMageHero.equipArmor(Slot.Legs, armor2);
        myFirstMageHero.equipArmor(Slot.Head, armor3);
        HeroAttribute calculatedAttributes = myFirstMageHero.CalculateTotalAttributes();
        HeroAttribute expectedHero = new HeroAttribute(4,12,29);

        Assert.assertEquals(expectedHero.getStrength(), calculatedAttributes.getStrength());
        Assert.assertEquals(expectedHero.getDexterity(), calculatedAttributes.getDexterity());
        Assert.assertEquals(expectedHero.getIntelligence(), calculatedAttributes.getIntelligence());
    }
    @Test
    public void TotalArmorAttributes_WithThreeEquipmentsAndReplace_ShouldBeCalculatedCorrectly() throws InvalidArmorException {
        Hero myFirstMageHero =  new Mage("Marco");
        Armor armor1 = new Armor("Cloak",1,Slot.Body,ArmorType.Cloth,1,5,7);
        Armor armor2 = new Armor("flip flops",1,Slot.Legs,ArmorType.Cloth,1,2,5);
        Armor armor3 = new Armor("Wooden Crown",1,Slot.Head,ArmorType.Cloth,1,4,9);
        Armor ReplaceArmor = new Armor("Sandals",1,Slot.Legs,ArmorType.Cloth,5,9,1);
        myFirstMageHero.equipArmor(Slot.Body, armor1);
        myFirstMageHero.equipArmor(Slot.Legs, armor2);
        myFirstMageHero.equipArmor(Slot.Head, armor3);
        myFirstMageHero.equipArmor(Slot.Legs, ReplaceArmor);
        HeroAttribute calculatedAttributes = myFirstMageHero.CalculateTotalAttributes();
        HeroAttribute expectedHero = new HeroAttribute(8,19,25);

        Assert.assertEquals(expectedHero.getStrength(), calculatedAttributes.getStrength());
        Assert.assertEquals(expectedHero.getDexterity(), calculatedAttributes.getDexterity());
        Assert.assertEquals(expectedHero.getIntelligence(), calculatedAttributes.getIntelligence());
    }
    @Test
    public void TotalWeaponDamage_WithNoWeapon_ShouldBeCalculatedCorrectly() throws InvalidWeaponException{
        Hero myFirstMageHero =  new Mage("Marco");
        double actualDamage = myFirstMageHero.getHeroDamage();
        Assert.assertEquals(1.17 ,actualDamage, 0);

    }
    @Test
    public void TotalWeaponDamage_WithOneWeapon_ShouldBeCalculatedCorrectly() throws InvalidWeaponException{
        Hero myFirstMageHero =  new Mage("Marco");
        Weapon weapon1 = new Weapon("woodenWand", 1, WeaponType.Wands,20);
        myFirstMageHero.equipWeapon(weapon1);
        double actualDamage = myFirstMageHero.getHeroDamage();
        Assert.assertEquals(21.6 ,actualDamage, 0);

    }
    @Test
    public void TotalWeaponDamage_WithOneWeaponAndReplace_ShouldBeCalculatedCorrectly() throws InvalidWeaponException{
        Hero myFirstWarriorHero =  new Warrior("Marco");
        Weapon weapon1 = new Weapon("Axe", 1, WeaponType.Axes,60);
        Weapon weapon2 = new Weapon("Sword", 1,WeaponType.Swords, 55);
        myFirstWarriorHero.equipWeapon(weapon1);
        myFirstWarriorHero.equipWeapon(weapon2);
        double actualDamage = myFirstWarriorHero.getHeroDamage();
        Assert.assertEquals(57.75 ,actualDamage, 0);
    }

    @Test
    public void TotalWeaponDamage_WithOneWeaponAndArmor_ShouldBeCalculatedCorrectly() throws InvalidWeaponException, InvalidArmorException {
        Hero myFirstWarriorHero =  new Warrior("Marco");
        Weapon weapon1 = new Weapon("Axe", 1, WeaponType.Axes,60);
        Armor armor1 = new Armor("Iron Plate", 1,Slot.Body,ArmorType.Plate,7,2,3);
        myFirstWarriorHero.equipWeapon(weapon1);
        myFirstWarriorHero.equipArmor(Slot.Body,armor1);
        double actualDamage = myFirstWarriorHero.getHeroDamage();
        Assert.assertEquals(63.0 ,actualDamage, 0);
    }




}