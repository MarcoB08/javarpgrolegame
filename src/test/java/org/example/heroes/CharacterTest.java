package org.example.heroes;
import org.junit.Assert;
import org.junit.Test;

public class CharacterTest {
    @Test
    public void TestBeginStage_Mage(){
        Hero myFirstMageHero =  new Mage("Marco");
        Assert.assertEquals(1, myFirstMageHero.getLevel());
        Assert.assertEquals(1, myFirstMageHero.getLevelAttributes().getStrength());
        Assert.assertEquals(1, myFirstMageHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(8, myFirstMageHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestBeginStage_Ranger(){
        Hero myFirstRangerHero =  new Ranger("Marco");
        Assert.assertEquals(1, myFirstRangerHero.getLevel());
        Assert.assertEquals(1, myFirstRangerHero.getLevelAttributes().getStrength());
        Assert.assertEquals(7, myFirstRangerHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(1, myFirstRangerHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestBeginStage_Rogue(){
        Hero myFirstRogueHero =  new Rogue("Marco");
        Assert.assertEquals(1, myFirstRogueHero.getLevel());
        Assert.assertEquals(2, myFirstRogueHero.getLevelAttributes().getStrength());
        Assert.assertEquals(6, myFirstRogueHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(1, myFirstRogueHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestBeginStage_Warrior(){
        Hero myFirstWarriorHero =  new Warrior("Marco");
        Assert.assertEquals(1, myFirstWarriorHero.getLevel());
        Assert.assertEquals(5, myFirstWarriorHero.getLevelAttributes().getStrength());
        Assert.assertEquals(2, myFirstWarriorHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(1, myFirstWarriorHero.getLevelAttributes().getIntelligence());
    }
    @Test
    public void TestLevelUp_Mage(){
        Hero myFirstMageHero =  new Mage("Marco");
        myFirstMageHero.levelUp();
        Assert.assertEquals(2, myFirstMageHero.getLevel());
        Assert.assertEquals(2, myFirstMageHero.getLevelAttributes().getStrength());
        Assert.assertEquals(2, myFirstMageHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(13, myFirstMageHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestLevelUp_Ranger(){
        Hero myFirstRangerHero =  new Ranger("Marco");
        myFirstRangerHero.levelUp();
        Assert.assertEquals(2, myFirstRangerHero.getLevel());
        Assert.assertEquals(2, myFirstRangerHero.getLevelAttributes().getStrength());
        Assert.assertEquals(12, myFirstRangerHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(2, myFirstRangerHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestLevelUp_Rogue(){
        Hero myFirstRogueHero =  new Rogue("Marco");
        myFirstRogueHero.levelUp();
        Assert.assertEquals(2, myFirstRogueHero.getLevel());
        Assert.assertEquals(3, myFirstRogueHero.getLevelAttributes().getStrength());
        Assert.assertEquals(10, myFirstRogueHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(2, myFirstRogueHero.getLevelAttributes().getIntelligence());
    }

    @Test
    public void TestLevelUp_Warrior(){
        Hero myFirstWarriorHero =  new Warrior("Marco");
        myFirstWarriorHero.levelUp();
        Assert.assertEquals(2, myFirstWarriorHero.getLevel());
        Assert.assertEquals(8, myFirstWarriorHero.getLevelAttributes().getStrength());
        Assert.assertEquals(4, myFirstWarriorHero.getLevelAttributes().getDexterity());
        Assert.assertEquals(2, myFirstWarriorHero.getLevelAttributes().getIntelligence());
    }
}