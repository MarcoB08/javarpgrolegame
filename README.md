# Assignment 1 Build a RPG console application in Java

This project is a console application that was created for a Java Assignment.
This application allows users to choose a Hero and equip weapon and armor. 
You can choose between different subclasses.

## Subclasses
* Mage
* Ranger
* Rogue
* Warrior

## Methods
* levelUp() : LevelUp your own Hero
* equipWeapon() : Equip a weapon for your hero
* equipArmor() : Equip an armor piece for you hero
* displayHeroStats() : Display all stats of the hero

### IDE software
To run and use this project you can use a software that you like. 
This project was build in IntelliJ.


